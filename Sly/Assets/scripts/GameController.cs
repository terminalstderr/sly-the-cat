﻿using System;
using UnityEngine;

delegate void ScoreChangeDelegate(int score);

internal class GameController : Singleton<GameController>
{
    public CatMoveAi CatAI;
    public GameObject Player;
    private static GameController instance;
    private int score = 0;
    private ScoreChangeDelegate score_change_delegate;

    public void AddPoints(int points) {
        score += points;
        score_change_delegate(score);
    }

    public void AddScoreChangeDelegate(ScoreChangeDelegate scd) {
        score_change_delegate += scd;
    }
}

