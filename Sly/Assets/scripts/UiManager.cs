﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UiManager : MonoBehaviour {
    private GameController gc;
    public Text score_text;
	// Use this for initialization
	void Start () {
        gc = GameController.Instance;
        gc.AddScoreChangeDelegate(this.UpdateScore);
    }

    private void UpdateScore(int score)
    {
        score_text.text = "Score: " + score;
    }
}
