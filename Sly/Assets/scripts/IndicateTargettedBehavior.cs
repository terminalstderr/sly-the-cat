﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicateTargettedBehavior : MonoBehaviour {
    public ParticleSystem _particles;
    private int _breakables_before_me;

    // TODO There is a particle effect for being up next, but also 2nd, and 3rd.
    // The logic for 1st, 2nd, 3rd will be handled by the CatAI, but we will
    // provide methods here for all 3... Actually, lets make it based on an
    // algorithm that takes an 'index' to indicate how far away the object is
    // TODO Particle effect changes intensity when the cat gets nearer

    void UpdateParticleSystem() {
        // Update this particle system based on
        var ps = GetParticleSystem();
        var intensity = CalculateIntensity();
        ps.startColor = (Color.red * intensity);
        ps.simulationSpeed = intensity * 5f;

    }

    private float CalculateIntensity() {
        // The algorithm is inversly exponential based on how far the cat is
        // Based on how many are before it
        var intensity = 0f;
        if (_breakables_before_me == 0)
        {
            intensity = 1f;
        }
        else
        {
            intensity = Mathf.Pow(1f / (_breakables_before_me + 1), 4);
        }
        return intensity;
    }

    private ParticleSystem.MainModule GetParticleSystem(){
        if (_particles == null)
            _particles = GetComponentInChildren<ParticleSystem>();
        return _particles.main;
    }

    public void PrimaryTargetAfter(int breakables_targetted_before_me_count) {
        _breakables_before_me = breakables_targetted_before_me_count;
        UpdateParticleSystem();
    }

}
