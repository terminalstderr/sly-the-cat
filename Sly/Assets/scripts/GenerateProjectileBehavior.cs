﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateProjectileBehavior : MonoBehaviour {
    public Rigidbody projectile;
    public float projectile_speed;
    public Transform aim_point;
    public Transform start_point;
    public string shoot_key;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        bool shoot = Input.GetButtonDown(shoot_key);
        if (shoot)
        {
            Rigidbody projectile_instance = Instantiate(projectile, 
                                                        aim_point.position, 
                                                        Quaternion.identity);
            projectile_instance.AddForce((aim_point.position - start_point.position) * projectile_speed);
        }
		
	}
}
