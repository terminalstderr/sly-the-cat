using System;
using UnityEngine;

/*
 * Based off of Unity Standard
 */
public class AimBehavior : MonoBehaviour
{
    [Range(0f, 10f)] [SerializeField] private float m_TurnSpeed = 1.5f;   // How fast the rig will rotate from user input.
    [SerializeField] private float m_TiltMax = 75f;                       // The maximum value of the x axis rotation of the pivot.
    [SerializeField] private float m_TiltMin = 45f;                       // The minimum value of the x axis rotation of the pivot.
    [SerializeField] private bool m_LockCursor = false;                   // Whether the cursor should be hidden and locked.

    // Working variables, initialized during 'Awake' and used during 'Update'
    private float m_LookAngle;                    // The rig's y axis rotation.
    private float m_TiltAngle;                    // The pivot's x axis rotation.
	private Quaternion m_TransformTargetRot;

    private void Awake()
    {
        // Lock or unlock the cursor.
        Cursor.lockState = m_LockCursor ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !m_LockCursor;

		m_TransformTargetRot = transform.localRotation;
    }


    private void Update()
    {
        HandleRotationMovement();
        if (m_LockCursor && Input.GetMouseButtonUp(0))
        {
            Cursor.lockState = m_LockCursor ? CursorLockMode.Locked : CursorLockMode.None;
            Cursor.visible = !m_LockCursor;
        }
    }

    private void OnDisable()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void HandleRotationMovement()
    {
		if(Time.timeScale < float.Epsilon)
		return;

        // Read the user input
        var x = Input.GetAxis("Mouse X");
        var y = Input.GetAxis("Mouse Y");

        // Adjust the look angle by an amount proportional to the turn speed and horizontal input.
        m_LookAngle += x*m_TurnSpeed;

        // Rotate the rig (the root object) around Y axis only:
        //m_TransformTargetRot = Quaternion.Euler(0f, m_LookAngle, 0f);


        // adjust the current angle based on Y mouse input and turn speed
        m_TiltAngle -= y*m_TurnSpeed;
        // and make sure the new value is within the tilt range
        m_TiltAngle = Mathf.Clamp(m_TiltAngle, -m_TiltMin, m_TiltMax);

        m_TransformTargetRot = Quaternion.Euler(m_TiltAngle, m_LookAngle, 0f);

		transform.localRotation = m_TransformTargetRot;
    }
}
