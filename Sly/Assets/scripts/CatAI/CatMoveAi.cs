﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CatMoveAi : MonoBehaviour {

    NavMeshAgent agent;
    public GameObject path_game_object;
    public string target_tag;

    public List<Transform> PathContainer = new List<Transform>();

    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();

        // Setup the path based on the path container
        foreach (Transform child in path_game_object.transform)
        {
            if (child.CompareTag(target_tag))
            {
                PathContainer.Add(child);
            }
        }

        // Prime the pump! Get the AI running!
        UpdateAi();
    }

    // If a vase has been either gummed by the player, or taken out by the cat,
    // it needs to be removed from the AI path, and the AI path needs to be re-
    // evaluated.
    public void RemoveObjectFromPathAndUpdateAi(Transform transform_to_remove)
    {
        // Remove the transform
        PathContainer.Remove(transform_to_remove);

        UpdateAi();
    }

    private void UpdateAi()
    {
        if (PathContainer.Count > 0){
            var next_target = PathContainer[0];
            agent.SetDestination(next_target.position);

            // Let all the breakables know which is targetted next...
            var i = 0;
            foreach (Transform targetted in PathContainer) {
                var b = targetted.GetComponent<IndicateTargettedBehavior>();
                b.PrimaryTargetAfter(i);
                i++;
            }
        }

        // TODO Indicate to the game manager that the level is over... the cat
        // has finished its work!
    }


}
