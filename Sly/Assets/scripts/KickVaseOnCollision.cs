﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KickVaseOnCollision : MonoBehaviour {

    // TODO on collision with a 'breakable', cause it to fling off the shelf 
    // and toward the player.
    // TODO on collision with a 'breakable', remove it from the cat AI

    public float KickSpeed;

    public void OnCollisionEnter(Collision collision)
    {
        _HandleCollision(collision.collider.gameObject);
    }

    public void OnTriggerEnter(Collider other)
    {
        _HandleCollision(other.gameObject);
    }

    private void _HandleCollision(GameObject other)
    {
        // Only kick breakables (currently only vases are breakable)
        if (other.CompareTag("vase"))
        {
            // Only kick a breakable that hasn't been touched yet
            if (other.GetComponent<BreakableStateBehavior>().State == BreakableState.untouched)
            {
                // Add a force to the breakable; kick the vase to the ground toward the player!
                var o_rb = other.GetComponent<Rigidbody>();
                Vector3 direction_vase_to_player = GameController.Instance.Player.transform.position - other.transform.position;
                direction_vase_to_player.Normalize();
                o_rb.AddForce(direction_vase_to_player * KickSpeed);


                // Remove from the cat AI; let the cat know the vase has been taken care of!
                GameController.Instance.CatAI.RemoveObjectFromPathAndUpdateAi(other.transform);
            }
        }
    }
}
