﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchVaseBehavior : MonoBehaviour {
    private GameController gc;
    public int points_to_add;

	// Use this for initialization
	void Start () {
        gc = GameController.Instance;
	}

    public void OnCollisionEnter(Collision collision)
    {
        _HandleCollision(collision.collider.gameObject);
    }

    public void OnTriggerEnter(Collider other)
    {
        _HandleCollision(other.gameObject);
    }

    private void _HandleCollision(GameObject other) {
        if (other.CompareTag("vase"))
        {
            // Set inactive
            other.SetActive(false);

            // Update points appropriately
            gc.AddPoints(points_to_add);

            // Let the cat know the vase was caught!
            gc.CatAI.RemoveObjectFromPathAndUpdateAi(other.transform);
        }
    }
}
    