﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BreakableState
{
    untouched,
    kicked,
    gummed
};

public class BreakableStateBehavior : MonoBehaviour {
    public BreakableState State = BreakableState.untouched;
}
